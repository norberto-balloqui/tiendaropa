<?php

require_once("Conexion.php");


$ConsultaPedidoSql = 
"SELECT ti.tie_id, ti.tie_cantidad, ti.ped_id, ti.pro_id, 
pr.pro_nombre, p.ped_id 
FROM TIENE as ti
INNER JOIN PEDIDO AS p ON ti.ped_id=p.ped_id
INNER JOIN PRODUCTO AS pr ON ti.pro_id=pr.pro_id 
WHERE p.ped_id IN 
   ( SELECT p.ped_id,  c.cli_nombre 
      FROM PEDIDO as p
      INNER JOIN CLIENTE AS c ON p.cli_id=c.cli_id)
";
$ConsultaPedido = mysqli_query($con, $ConsultaPedidoSql);



?>