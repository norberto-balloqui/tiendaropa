
<?php
include("Conexion.php");
?>

<!DOCTYPE html>
<html lang="es">
<head>

    <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Crear Reunion</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"> 
    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <!-- Datepicker -->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

<!-- estilo -->
    <link rel="stylesheet" type="text/css" href="../estilo.css">
</head>
<body style="background-color:#5e42a6">

<!-- Formulario -->
<br>
<div style="text-align: center;"><h1>Registrar Nueva Institución</h1>

<br>
	<div class="container">
		<div class="row">
			<div class="col-12">
			<form class="form" action="CrearInstitucion.php" method="POST" role="form" autocomplete="off">

    <div class="form-group row">
		<label class="col-lg-3 col-form-label form-control-label">Ingrese nombre de la institución</label>
		<div class="col-lg-8">
			<input class="form-control" type="text" name="ins_nombre" id="ins_nombre" 
			placeholder="Se puede ingresar solo números y letras"  minlength="2" maxlength="35" required="" pattern="[a-z A-ZñÑáéíóúÁÉÍÓÚ0-9]{2,35}" >
		</div>
	</div>

	<div class="form-group row">
		<label class="col-lg-3 col-form-label form-control-label">Ingrese lugar perteneciente de la institución</label>
		<div class="col-lg-8">
			<input class="form-control" type="text" name="ins_lugar" id="ins_lugar" placeholder="Se puede ingresar solo números y letras" minlength="2" maxlength="35" required="" pattern="[a-z A-ZñÑáéíóúÁÉÍÓÚ0-9]{2,35}">
		</div>
    </div>

	<div class="form-group row">
		<div class="col-lg-12 text-center">
			<input type="submit" class="btn btn-primary"
				value="Guardar Institución" >
				    &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
            <a type="submit"  class="btn btn-primary" href="./inicio.php">Regresar</a>
		</div>
	</div>
</form>



		</div>
		</div>

	</div>
  </div>




   
    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <!-- Bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <!-- Datepicker -->
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    
    <script>
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });

        $('#datepicker2').datepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>

</body>
</html>