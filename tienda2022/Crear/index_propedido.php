

<?php
    require_once('conexionpdo.php');

    
    $sql = "SELECT ped_id FROM PEDIDO ORDER BY ped_id DESC";
    $sql=$pdo->prepare($sql);
    $sql->execute();
    $resultado=$sql->fetchAll();

    $sql2 = "SELECT pro_id, pro_nombre FROM PRODUCTO ORDER BY pro_nombre ASC";
    $sql2=$pdo->prepare($sql2);
    $sql2->execute();
    $resultado2=$sql2->fetchAll();

?>

<!DOCTYPE html>
<html lang="es">
<head>
<meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Crear propedido</title>

    <!-- Jquery 3.1.1-->
    <script language="=javascript" src="js/jquery-3.1.1.min.js"></script>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"> 
    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <!-- Datepicker -->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <!-- scrpit -->
    <script language="javascript" type="text/javascript" ></script>
    <!-- estilo -->
    <link rel="stylesheet" type="text/css" href="../estilo.css">

</head>
<body style="background-color:#5e42a6">

<!-- Formulario -->
<br>
<div style="text-align: center;"><h1>Agregar producto al pedido</h1>

<br>
	<div class="container">
		<div class="row">
			<div class="col-12">
			<form class="form" action="CrearProPedido.php" method="POST" role="form" autocomplete="off">

   

     <div class="form-group row">
         <label for="validationTooltip04" class="col-lg-2 col-form-label form-control-label"></label>
        <div class="col-lg-8"> Seleccione el codigo del pedido
             <select class="form-control" name="ped_id" id="validationTooltip04" required>
             <option selected disabled value="">Seleccionar</option>

            <?php for($i=0; $i<count($resultado);$i++) { ?>
                    <option value="<?php echo $resultado[$i]['ped_id']; ?>"><?php echo utf8_encode($resultado[$i]['ped_id']); ?></option>
                <?php } ?>        
             </select>
             <div class="invalid-tooltip">
      Seleccionar una estado valido
             </div>
        </div>
    </div>

<br>
    <div class="form-group row">
        <label for="validationTooltip04" class="col-lg-2 col-form-label form-control-label"></label>
        <div class="col-lg-8"> Seleccione el producto que se agregara al pedido
            <select class="form-control" name="pro_id" id="validationTooltip04" required>
            <option selected disabled value="">Seleccionar</option>

            <?php for($i=0; $i<count($resultado2);$i++) { ?>
                    <option value="<?php echo $resultado2[$i]['pro_id']; ?>"><?php echo utf8_encode($resultado2[$i]['pro_nombre']); ?></option>
                <?php } ?>

           </select>
           <div class="invalid-tooltip">
      Seleccionar una estado valido
             </div>

        </div>
    </div>

    <br><br>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label form-control-label">Ingrese cantidad de unidades del producto</label>
        <div class="col-lg-8">
            <input class="form-control" type="int" name="tie_cantidad" id="tie_cantidad"  required="" placeholder="Se puede ingresar solo números" minlength="1" maxlength="7" pattern="[0-9.]{1,7}" >
        </div>
    </div>


<br>
	<div class="form-group row">
		<div class="col-lg-12 text-center">
			<input type="submit" class="btn btn-primary"
				value="Guardar Producto" >

                &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
            <a type="submit"  class="btn btn-primary" href="./inicio.php">Regresar</a>
		</div>
	</div>
</form>



		</div>
		</div>

	</div>
  </div>




   
    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <!-- Bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <!-- Datepicker -->
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    
    <script>
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });

        $('#datepicker2').datepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>

</body>
</html>