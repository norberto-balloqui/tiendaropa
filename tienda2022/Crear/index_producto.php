

<?php
    require_once('conexionpdo.php');

    
    $sql = "SELECT ins_id, ins_nombre FROM INSTITUCION ORDER BY ins_nombre ASC";
    $sql=$pdo->prepare($sql);
    $sql->execute();
    
    $resultado=$sql->fetchAll();
   
    $sql2 = "SELECT tall_id, tall_nombre FROM TALLA ORDER BY tall_nombre ASC";
    $sql2=$pdo->prepare($sql2);
    $sql2->execute();
    $resultado2=$sql2->fetchAll();
    
    header("Content-Type: text/html; charset=utf-8");
?>

<!DOCTYPE html>
<html lang="es">
<head>
<meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Crear producto</title>

    <!-- Jquery 3.1.1-->
    <script language="=javascript" src="js/jquery-3.1.1.min.js"></script>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"> 
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>    
    <!-- select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link href="https://raw.githack.com/ttskch/select2-bootstrap4-theme/master/dist/select2-bootstrap4.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <!-- Datepicker -->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <!-- scrpit -->
    <script language="javascript" type="text/javascript" ></script>
    <!-- estilo -->
    <link rel="stylesheet" type="text/css" href="../estilo.css">

</head>
<body style="background-color:#5e42a6">

<!-- Formulario -->
<br>
<div style="text-align: center;"><h1>Registrar producto nuevo</h1>

<br>
  <div class="container">
        <div class="row">
            <div class="col-12">
<form class="form" action="CrearProducto.php" id="combo" name="combo" method="POST" role="form" autocomplete="off">

    <div class="form-group row">
        <label class="col-lg-3 col-form-label form-control-label">Ingrese el nombre del producto</label>
        <div class="col-lg-8">
            <input class="form-control" type="text" name="pro_nombre" id="pro_nombre" placeholder="Se puede ingresar solo números y letras"  minlength="1" maxlength="35" required="" pattern="[a-z A-ZñÑáéíóúÁÉÍÓÚ0-9]{1,35}" >

            
        </div>
    </div>


    <div class="form-group row">
        <label class="col-lg-3 col-form-label form-control-label">Ingrese el precio del producto</label>
        <div class="col-lg-8">
            <input class="form-control" type="int" name="pro_precio" id="pro_precio" placeholder="Se puede ingresar solo números" minlength="1" maxlength="8" pattern="[0-9.]{1,8}" required >


        </div>
    </div>


    <div class="form-group row">
           <label for="validationTooltip04" class="col-lg-2 col-form-label form-control-label"></label>
        <div class="col-lg-8"> Seleccionar institución
             <select required aria-required="true"
              class="form-control" name="ins_id"  id="validationTooltip04" required>
             
             <option selected disabled value="">Seleccionar</option>

            <?php for($i=0; $i<count($resultado);$i++) { ?>
                    
                <option value="<?php echo $resultado[$i]['ins_id']; ?>"><?php echo utf8_encode($resultado[$i]['ins_nombre']); ?></option>


                <?php } 
                
                ?>        
             </select>
             <div class="invalid-tooltip">
      Seleccionar una institución valida
             </div>
        </div>
    </div>




    <div class="form-group row">
        <label for="validationTooltip04" class="col-lg-2 col-form-label form-control-label"></label>

        <div class="col-lg-8"> Seleccionar talla
            <select required aria-required="true"
             class="form-control" name="tall_id" id="validationTooltip04" required>
            
             <option selected disabled value="">Seleccionar</option>

            <?php for($i=0; $i<count($resultado2);$i++) { ?>
                    <option value="<?php echo $resultado2[$i]['tall_id']; ?>"><?php echo utf8_encode($resultado2[$i]['tall_nombre']); ?></option>
               
                    <?php } ?>
         
           </select>
           <div class="invalid-tooltip">
      Seleccionar una talla valida
             </div>

        </div>
    </div>

    <div class="form-group row">
        <div class="col-lg-12 text-center">
            <input type="submit" class="btn btn-primary"
                value="Guardar Producto" >

                    &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
            <a type="submit"  class="btn btn-primary" href="./inicio.php">Regresar</a>
        </div>
    </div>
</form>



        </div>
        </div>

    </div>
  </div>




   
    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <!-- Bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <!-- Datepicker -->
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    
    <script>
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });

        $('#datepicker2').datepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>
  
     <!-- Select2 -->
    <script>

    $(document).ready(function(){

     $('.select2').select2({
    placeholder:'Select Category',
    theme:'bootstrap4',
    tags:true,
      });
    
    

});

</script>


</body>
</html>