$(function(){

  $('#datepicker').datepicker({
     uiLibrary: 'bootstrap4'
  });

  $('#datepicker2').datepicker({
      uiLibrary: 'bootstrap4'
  });

  $("#menu-toggle").click(function(e) {
  e.preventDefault();
  $("#wrapper").toggleClass("toggled");
  });



  
///Acta modal mostrar
  $('.editar2').click(function(){
    
    var editar2 = {

      a_id: $(this).data('a_id'),
      a_descripcion: $(this).data('a_descripcion'),
      arc_id: $(this).data('arc_id'),
      r_id: $(this).data('r_id'),
      
    }
    
    mostrarDatosMoActa(editar2);
    
  });




  
  //Reunion modal mostrar
  $('.editar').click(function(){
    
    var editar = {

      ped_id: $(this).data('ped_id'),
      ped_fecha_reg: $(this).data('ped_fecha_reg'),
      ped_fecha_des: $(this).data('ped_fecha_des'),
      ven_precio_total: $(this).data('ven_precio_total'),
      ven_abono: $(this).data('ven_abono'),
      ped_comentario: $(this).data('ped_comentario'),
      cli_id: $(this).data('cli_id'),
      est_id: $(this).data('est_id')
    }
    
    mostrarDatosEnModal(editar);
  });
  
  
  $('#guardarActaEditar').click(function(){
    
    GuardarActaEditar($(this));

  });
  
  
  
  $('#guardarReunionEditar').click(function(){

      GuardarReunionEditar($(this));

    });

  
  $('#mydatatable').DataTable({
    "language": {
        "sProcessing":     "Procesando...",
        "sLengthMenu":	   "Mostrarme _MENU_ los registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar los datos :&nbsp;&nbsp;",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
        },
        "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
      }
});
});


//Acta mostrar datos modal
function mostrarDatosMoActa(editar2){
 
$('#a_idModal').val(editar2.a_id);
$('#a_descripcionModal').val(editar2.a_descripcion);
$('#arc_idModal').val(editar2.arc_id);
$('#r_idModal').val(editar2.r_id);

}



//Reunion mostrar datos modal 
function mostrarDatosEnModal(editar){
 
$('#r_idModal').val(editar.r_id);
$('#r_temaModal').val(editar.r_tema);
$('#datepicker2').val(editar.r_fecha);
$('#r_horaModal').val(editar.r_hora);
$('#r_descripcionModal').val(editar.r_descripcion);
$('#r_lugarModal').val(editar.r_lugar);
$('#c_idModal').val(editar.c_id);
}

function GuardarActaEditar(btn){

btn.prop('disabled', true);

var editarDatosAct = LeerDatosActa();




$.ajax({
type: "POST",
data: editarDatosAct,
url: 'modificarR/ModificarActa.php',

  success: function(){
  


    btn.prop('disabled', false);
    $('#EditarReunion').modal('hide');
    location.reload();
  },
  error: function(){
    console.log("No se ha podido enviar la informacion");
  }
})

}






function GuardarReunionEditar(btn){

btn.prop('disabled', true);

var editarDatos = LeerDatosEdicion();
 

console.log("Hola estoy imprimiendo datos" + editarDatos.r_idModal);

$.ajax({
type: "POST",
data: editarDatos,
url: 'Modificar/ModificarPedido.php',

  success: function(){
  

    
    btn.prop('disabled', false);
    $('#EditarReunion').modal('hide');
    location.reload();
  },
  error: function(){
    console.log("No se ha podido enviar la informacion");
  }
})

}

//leer Acta
function LeerDatosActa(){

let datos = {

  a_idModal : $('#a_idModal').val().trim(),
  a_descripcionModal: $('#a_descripcionModal').val().trim(),
  arc_idModal: $('#arc_idModal').val().trim(),
  r_idModal: $('#r_idModal').val().trim(),

}

return datos;

}



//Leer Reunion
function LeerDatosEdicion(){

var datos = {

  ped_idModal : $('#ped_idModal').val().trim(),
  pedidofecha: $('#datepicker2').val().trim(),
  retirarfecha: $('#datepicker2').val().trim(),
  ven_precio_totalModal: $('#ven_precio_totalModal').val().trim(),
  ven_abonoModal: $('#ven_abonoModal').val().trim(),
  ped_comentarioModal: $('#ped_comentarioModal').val().trim(),
  cli_idModal: $('#pcli_idModal').val().trim(),
  est_idModal: $('#est_idModal').val().trim(),
}

return datos;

}




