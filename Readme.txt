Sistema de apoyo a tienda de buzos
Descripción del proyecto

Este proyecto apoya al sistema de pedidos de una tienda de buzos de la señora Susana. El software ayuda a guardar pedidos de las ventas realizadas en la tienda, guardando detalles de esta. Resuelve el proceso que se hacia escribiendo en cuaderno fisico.

Sus caracteristicas son.
Guardar y ver.:

-Inicio:
 Pedidos

-administrador:
 Produtos
 clientes
 Institución
 Tallas
 Estados
 Propedido(productos asignados a pedido)


Software stack
El proyecto tienda2022 es una aplicación web que corre en el siguiente software:


Ubuntu 18.04
Apache 2.4.29 
PHP 7.2 (ext:  mysql, zip)
Base de Datos MySQL 5




IMPORTANTE: Por razones de Seguridad NUNCA debes guardar las credenciales y subirlas al repositorio




Instalación server de la Server U:

Si estas fuera de la U se necesita estar conectado a  vpn dado por la U.

Entrar en putty:
Colocar su IP host y su puerto SSH

Al entrar loguearse con usuario y pasword.

Entrar modo su root y ingresar contraseña de administrador

Empezamos instalando GIT.

Primero colocar:
sudo apt update

sudo apt install git
y (aceptar)

revizar version git si fuera necesario
git --version


Instalar apache2 y php 7.2:

Colocar:
apt update && apt-get -y install apache2 php7.2 php7.2-mysql libapache2-mod-php7.2


Si aparece zona de horario:

sale zona horaria colocar 
america es 2, 
argentina es 6,
apretar enter para bajar, 
zone hora colocar 6

abrimos los permisos
chmod 777 -R /var/www/html/

Empezamos apache2
service apache2 start

Nos dirigimos a la carpeta 

var/www/html 

Clonamos repositorio 

git clone https://gitlab.com/norberto-balloqui/tiendaropa.git


PAra Revizar pagina:
IP:puerto apache

Ejemplo:
146.83.194.142:1042

Revizar si esta instalado un editor de texto:
nano --version

Si no hay uno instalado colocar siguiente comando para instalarlo:
sudo apt-get install nano


FAltan 2 puntos necesarios  para mostrar el proyecto correctamente. Las cuales son
 1-poblar la BD y 2-modificar el archivo Conexion/conexionpdo.


1-Poblar BD Sql:
Primero entrar a la BD colocando usuario y su contraseña,

Al creary entrar a la BD tener listo para ejecutar una consulta.
Esta consulta se encuentra en el archivo sql_tienda_server_UBB.txt en la raiz.
Este contenido se copia y pega, despues ejecuta la consulta, con eso se poblara con entidades y sus atributos, relaciones, iserts necesarios.


2-Cambios necesarios en el proyecto  para que se pueda conectar:

Dentro del proyecto de la carpeta tienda2022 hay varios archivos con el nombre Conexion.php y conexionpdo.php

 
Estos se cambian por los correspondientes a a la BD poblada nueva que se posee.

(PAra modificar sera necesario entrar con  nano nombre_de_archivo.php)

Lista de ubicación de estos archivos para modificar:

var/www/html/tienda2022 
modificar Conexion.php y conexionpdo.php

var/www/html/tienda2022/Crear
modificar Conexion.php y conexionpdo.php

var/www/html/tienda2022/Modulo
modificar Conexion.php


Ejemplo en el archivo Conexion.php se deben cambiar estos datos. (el ejemplo colocaron * para no mostrar mis credenciales, no hacer lo mismo)
Estos archivos hay que cambiar sus db_host (direccion de la BD) ,d_user (Usuario de la BD),db_pass (password de la BD), db_name (nombre de la BD poblada), 
db_port (puerto SSH).

$db_host = "mysql.******";
$db_user = "E20*****";
$db_pass = "E20****";
$db_name = "E20¨****";
$db_port = "1***";

En el archivo conexionpdo.php se deben cambiar estos:
Estos archivos hay que cambiar sus host (direccion de la BD) ,db (nombre de la BD poblada), user (Usuario de la BD),pass (password de la BD), 
port (puerto SSH).

$host = 'mysql******';
$db   = 'E20s*****';
$user = 'E20****';
$pass = 'E20******';
$port = "1***";

Con eso deberia leer la BD.

PAra Revizar pagina:
IP:puerto apache

Ejemplo:
146.83.194.142:1042



Instalación Docker:

1-
sudo apt update
2-
sudo apt install docker.io -y
3-
sudo nano /etc/sudoers
4-
Colocar alfinal del texto:

root ALL=(ALL) NOPASSWD: /usr/bin/dockerd


Cambiar permisos para permitir la correcta ejecución de la aplicación en entorno local

chmod -R 777 web/assets/ logs/ cache/


Con una terminal situarse dentro del directorio raiz donde fue clonado este repositorio.

En la misma ruta de el archivo Dockerfile:

hacemos la imagen colocando: 

sudo docker build -t servidor:1.0 .

despues

sudo docker run -d -p 80:80 servidor:1.0


La página final se visualizará en el siguiente link:


127.0.0.1:80/tienda2022/

(Si arroja otra ip cambiarla IPnueva:80)


Framework Usado
Bootstrap 4,5 - HTML, CSS, and JS Frontend Framework jQuery